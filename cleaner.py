import logging
import pathlib
import shutil
from collections import ChainMap

DEFAULT_CLEANING_PLAN = {
    "documents": [".txt", ".doc", ".docx", ".pdf"],
    "images": [".jpg", ".jpeg", ".png", ".gif"],
    "videos": [".avi", ".mp4", ".mov", ".mkv"],
    "music": [".mp3", ".ogg", ".wav", ".flac"],
    "archives": [".zip", ".tar", ".gz", ".rar"],
    "code": [".py", ".js", ".html", ".css"],
}


LOGGING_LEVEL = {
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARNING": logging.WARNING,
    "ERROR": logging.ERROR,
    "CRITICAL": logging.CRITICAL,
}


def get_logger(
        log_file_path: pathlib.Path,
        log_console_output_status: bool = True,
        log_file_output_status: bool = True,
        log_console_level: int = logging.WARNING,
        log_file_level: int = logging.DEBUG,
        log_file_mode: str = "w",
        **kwargs
) -> logging.Logger | None:
    """
    Створює логгер та налаштовує його.

    Логгер може виводити логи в консоль, файл або обидва.
    """
    if log_file_output_status or log_console_output_status:
        # створити логгер
        logger = logging.getLogger(__name__)
        # встановити рівень логування для логгера
        logger.setLevel(logging.DEBUG)
        # створити форматування для логування
        formatter = logging.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        )
        if log_console_output_status:
            # створити обробник для виводу логів в консоль
            console_handler = logging.StreamHandler()
            # встановити рівень логування для обробника
            console_handler.setLevel(log_console_level)
            # встановити форматування для обробника
            console_handler.setFormatter(formatter)
            # додати обробник до логгера
            logger.addHandler(console_handler)
        if log_file_output_status:
            # створити папку для логів
            log_file_path.parent.mkdir(parents=True, exist_ok=True)

            # створити обробник для виводу логів в файл
            file_handler = logging.FileHandler(log_file_path, mode=log_file_mode)
            # встановити рівень логування для обробника
            file_handler.setLevel(log_file_level)
            # встановити форматування для обробника
            file_handler.setFormatter(formatter)
            # додати обробник до логгера
            logger.addHandler(file_handler)
        return logger


def get_settings(
        source_dir: str,
        destination_dir: str | None = None,
        cleaning_plan: dict[str, list[str]] | None = None,
        unrecognized_file_name: str = "other",
        log_console_output_status: bool = True,
        log_file_output_status: bool = True,
        log_console_level: str = "WARNING",
        log_file_level: str = "DEBUG",
        log_file_name: str = "cleaner.log",
        log_folder_name: str | None = None,
        log_file_mode: str = "w",
        settings_file_name: str = "settings.toml",
        settings_folder_name: str | None = None,
        excludes: list[str] | None = None,
) -> dict:
    """
    Формує налаштування для роботи всього застосунка.

    Налаштування можуть бути отримані з:
    - CLI (найвищій пріорітет)
    - файлу налаштувань
    - значень за замовчуванням (найнижчій пріорітет)

    Після обʼєднання первинних налаштувань, вони обробляються і повертаються у вигляді словника.
    """
    default_settings = {
        "source_dir": source_dir,
        "destination_dir": destination_dir,
        "cleaning_plan": cleaning_plan,
        "unrecognized_file_name": unrecognized_file_name,
        "log_console_output_status": log_console_output_status,
        "log_file_output_status": log_file_output_status,
        "log_console_level": log_console_level,
        "log_file_level": log_file_level,
        "log_file_name": log_file_name,
        "log_folder_name": log_folder_name,
        "log_file_mode": log_file_mode,
        "settings_file_name": settings_file_name,
        "settings_folder_name": settings_folder_name,
        "excludes": excludes if excludes else [],
    }
    # отримати налаштування з CLI
    cli_settings = {}
    # отримати налаштування з файлу
    # шлях до файлу налаштувань шукати в файлі налаштувань - моветон) Він або командному рядку або за замовченням
    settings_folder_name = cli_settings.get("settings_folder_name") or default_settings.get("settings_folder_name")
    settings_file_name = cli_settings.get("settings_file_name") or default_settings.get("settings_file_name")
    if settings_folder_name:
        settings_file_path = (pathlib.Path(settings_folder_name) / settings_file_name).resolve()
    else:
        settings_file_path = (pathlib.Path(settings_file_name)).resolve()

    file_settings = {}
    # об'єднати налаштування
    raw_settings = dict(ChainMap(cli_settings, file_settings, default_settings))

    source_dir = pathlib.Path(raw_settings["source_dir"]).resolve()
    destination_dir = pathlib.Path(raw_settings["destination_dir"]).resolve() if raw_settings["destination_dir"] else source_dir
    if raw_settings["cleaning_plan"]:
        cleaning_plan = {(destination_dir / path).resolve(): indexes for path, indexes in raw_settings["cleaning_plan"].items()}
    else:
        cleaning_plan = {(destination_dir / path).resolve(): indexes for path, indexes in DEFAULT_CLEANING_PLAN.items()}

    if raw_settings["log_folder_name"]:
        log_file_path = (pathlib.Path(raw_settings["log_folder_name"]) / raw_settings["log_file_name"]).resolve()
    else:
        log_file_path = (destination_dir / raw_settings["log_file_name"]).resolve()

    excludes_set = set(pathlib.Path(path).resolve() for path in raw_settings["excludes"])
    excludes_set.add(settings_file_path)
    excludes_set.add(log_file_path)

    settings = {
        "source_dir": source_dir,
        "destination_dir": destination_dir,
        "cleaning_plan": cleaning_plan,
        "unrecognized_file": (destination_dir / unrecognized_file_name).resolve(),
        "log_console_output_status": raw_settings["log_console_output_status"],
        "log_file_path": log_file_path,
        "log_console_level": LOGGING_LEVEL[raw_settings["log_console_level"]],
        "log_file_level": LOGGING_LEVEL[raw_settings["log_file_level"]],
        "log_level_console": raw_settings["log_console_level"],
        "log_level_file": raw_settings["log_file_level"],
        "log_file_mode": raw_settings["log_file_mode"],
        "settngs_file_path": settings_file_path,
        "excludes": excludes_set,

    }

    return settings


def get_folder_path(
        file: pathlib.Path,
        cleaning_plan: dict[pathlib.Path, list[str]],
        unrecognized_file: pathlib.Path,
) -> pathlib.Path:
    for folder_path, extensions in cleaning_plan.items():
        if file.suffix in extensions:
            return folder_path
    return unrecognized_file


def create_plans(
        source_dir: pathlib.Path,
        cleaning_plan: dict[pathlib.Path, list[str]],
        unrecognized_file: pathlib.Path,
        **kwargs
) -> tuple[list[tuple[pathlib.Path, pathlib.Path]], list[pathlib.Path]]:
    transfer_plan = []
    delete_plan = []

    for filesystem_object in source_dir.rglob("*"):
        if filesystem_object.is_dir():
            delete_plan.append(filesystem_object)
        elif filesystem_object.is_file():
            transfer_plan.append(
                (
                    filesystem_object,
                    get_folder_path(filesystem_object, cleaning_plan, unrecognized_file)
                    / filesystem_object.name,
                )
            )
    return transfer_plan, delete_plan


def transfer_files(transfer_plan: list[(pathlib.Path, pathlib.Path)], logger: logging.Logger) -> tuple[int, int]:
    num_transferred_files = 0
    for source, destination in transfer_plan:
        copy_number = 1
        while destination.exists():
            destination = destination.with_name(
                f"{destination.stem}_{copy_number}{destination.suffix}"
            )
            copy_number += 1
        else:
            try:
                destination.parent.mkdir(parents=True, exist_ok=True)
                source.replace(destination)
                logger.info(f"Transferred {source} to {destination}")
                num_transferred_files += 1
            except Exception as e:
                logger.error(f"Error transferring {source} to {destination}: {e}")
    return num_transferred_files, len(transfer_plan)


def delete_dirs(delete_plan: list[pathlib.Path], logger: logging.Logger) -> tuple[int, int]:
    num_deleted_dirs = 0
    for directory in delete_plan:
        try:
            shutil.rmtree(directory)
            logger.info(f"Deleted {directory}")
            num_deleted_dirs += 1
        except Exception as e:
            logger.error(f"Error deleting {directory}: {e}")
    return num_deleted_dirs, len(delete_plan)


def cleaner():
    # отримати налаштування
    settings = get_settings(source_dir="test_folder", log_console_level="DEBUG")
    logger = get_logger(**settings)
    # створити план переносу файлів і видалення зайвих директорій
    transfer_plan, delete_plan = create_plans(**settings)
    print(transfer_plan)
    print(delete_plan)

    # виконати переноси файлів
    num_transferred_files, total_files = transfer_files(transfer_plan, logger)
    # видалити зайві директорії
    num_deleted_dirs, total_dirs = delete_dirs(delete_plan, logger)


if __name__ == "__main__":
    cleaner()
